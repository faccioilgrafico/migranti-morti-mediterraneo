	
	<title>La strage del Mediterraneo - Corriere.it</title>
	<meta name="title" content="La strage del Mediterraneo - Corriere.it">
	<meta name="description" content="Il Corriere della Sera, in collaborazione con Unhcr, pubblica online i dati dei migranti morti e dispersi nel Mediterraneo da ottobre 2013 a oggi">
	
	<meta property="fb:app_id" content="203568503078644"/>
	<meta property="og:title" content="La strage del Mediterraneo"/>
	<meta property="og:type" content="article"/>
	<meta property="og:locale" content="it_IT"/>
	<meta property="og:url" itemprop="url" content="//www.corriere.it/reportages/cronache/2016/migranti-morti-mediterraneo/"/>
	<meta property="og:image" content="//images2.corriereobjects.it/reportages/cronache/2016/migranti-morti-mediterraneo/img/social2.png"/>
	<meta property="og:site_name" content="Corriere della Sera"/>
	<meta property="og:description" content="Il Corriere della Sera, in collaborazione con Unhcr, pubblica online i dati dei migranti morti e dispersi nel Mediterraneo da ottobre 2013 a oggi. Il totale &egrave; di <!--#include virtual='include-morti.html' --> morti/dispersi"/>
	<meta name="author" content="Alessandra Coppola, Viviana Mazza, Marta Serafini, Federica Seneghini"/>

	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@Corriereit">
	<meta name="twitter:creator" content="Alessandra Coppola, Viviana Mazza, Marta Serafini, Federica Seneghini">
	<meta name="twitter:title" content="La strage del Mediterraneo">
	<meta name="twitter:description" content="Il Corriere della Sera, in collaborazione con Unhcr, pubblica online i dati dei migranti morti e dispersi nel Mediterraneo da ottobre 2013 a oggi. Il totale &egrave; di <!--#include virtual='include-morti.html' --> morti/dispersi">
	<meta name="twitter:image" content="//images2.corriereobjects.it/reportages/cronache/2016/migranti-morti-mediterraneo/img/social2.png">

	<link rel="canonical" href="//www.corriere.it/reportages/cronache/2016/migranti-morti-mediterraneo"/>

  <meta property="vr:canonical" content="//www.corriere.it/reportages/cronache/2016/migranti-morti-mediterraneo"/>
