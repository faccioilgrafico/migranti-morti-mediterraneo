	
	<title>Massacre in the Mediterranean - Corriere.it</title>
	<meta name="title" content="Massacre in the Mediterranean - Corriere.it">
	<meta name="description" content="">
	
	<meta property="fb:app_id" content="203568503078644"/>
	<meta property="og:title" content="Massacre in the Mediterranean"/>
	<meta property="og:type" content="article"/>
	<meta property="og:locale" content="it_IT"/>
	<meta property="og:url" itemprop="url" content="//www.corriere.it/reportages/cronache/2016/migranti-morti-mediterraneo/en"/>
	<meta property="og:image" content="//images2.corriereobjects.it/reportages/cronache/2016/migranti-morti-mediterraneo/img/social2.png"/>
	<meta property="og:site_name" content="Corriere della Sera"/>
	<meta property="og:description" content="<!--#include virtual='../include-morti.html' --> dead/missing"/>
	<meta name="author" content="Alessandra Coppola, Viviana Mazza, Marta Serafini, Federica Seneghini"/>

	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@Corriereit">
	<meta name="twitter:creator" content="Alessandra Coppola, Viviana Mazza, Marta Serafini, Federica Seneghini">
	<meta name="twitter:title" content="Massacre in the Mediterranean">
	<meta name="twitter:description" content="<!--#include virtual='../include-morti.html' --> dead/missing">
	<meta name="twitter:image" content="//images2.corriereobjects.it/reportages/cronache/2016/migranti-morti-mediterraneo/img/social2.png">

	<link rel="canonical" href="//www.corriere.it/reportages/cronache/2016/migranti-morti-mediterraneo/en"/>

  <meta property="vr:canonical" content="//www.corriere.it/reportages/cronache/2016/migranti-morti-mediterraneo/en"/>
