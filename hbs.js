var handlebars = require('handlebars');
var fs = require('fs');
var request = require("request");

//var url = 'http://corriere.s3.amazonaws.com/json-table/migranti-anagrafe-morti-nel-mediterraneo.json';
var url = 'https://www.corriere.it/infografiche/json-table/2016/migranti.json';
url+="?ie="+(new Date()).getTime();
var fooJson = {};

request({
    headers: {
      'User-Agent': 'request',
      'Cache-Control': 'private, no-cache, no-store, must-revalidate',
      'Expires': '-1',
      'Pragma': 'no-cache'
    },
    url: url,
    json: true
}, function (error, response, body) {
// test del push to deploy su ftp
    if (!error && response.statusCode === 200) {
        //console.log(body) // Print the json response
        //console.log(body.eventi[0].data_evento);
        data = body.eventi.sort(compareData);
        //console.log(data);
        //console.log(data[0].data_evento);
        //console.log(data[0].mese_evento);
        //console.log(data[0].anno_evento);

        _morti_totali = 0;

          elenco_mesi = {
            "gennaio" : 12,
            "febbraio" : 11,
            "marzo" : 10,
            "aprile" : 9,
            "maggio" : 8,
            "giugno" : 7,
            "luglio" : 6,
            "agosto" : 5,
            "settembre" : 4,
            "ottobre" : 3,
            "novembre" : 2,
            "dicembre" : 1
          };
          // filename_migranti = "http://corriere.s3.amazonaws.com/json-table/migranti-anagrafe-morti-nel-mediterraneo.json";

        
        _current_year = new Date().getFullYear();

        migranti_morti = { anni : {}, o : 0, c : 0, e : 0}; 
        // console.log(data);
        
        for (var eventi in data) {
                v = data[eventi];
                k = eventi;
                /*per fare il reverse degli anni calcolo il corrente - l'anno di riferimento*/
                migranti_morti[v.zona]+= parseInt(v.tot_morti);

                id_anno = _current_year - v.anno_evento;
                if( migranti_morti.anni[ id_anno ] == undefined)
                  migranti_morti.anni[ id_anno ] = { mesi : {} };
                if( migranti_morti.anni[ id_anno ].mesi[elenco_mesi[v.mese_evento]] == undefined)
                  migranti_morti.anni[ id_anno ].mesi[elenco_mesi[v.mese_evento]] = { eventi : [] };

                migranti_morti.anni[ id_anno ].mesi[elenco_mesi[v.mese_evento]]["anno_evento"] =  v.anno_evento ;
                migranti_morti.anni[ id_anno ].mesi[elenco_mesi[v.mese_evento]]["mese_evento"] =  v.mese_evento ;



                migranti_morti.anni[ id_anno ].mesi[elenco_mesi[v.mese_evento]].eventi.unshift(v);

                _morti_totali += parseInt(v.tot_morti);

                un = v.data_evento.substring(0, 1);
                if (un == '0') { 
                  un = '';
                }
                du = v.data_evento.substring(1, 2);
                console.log(un+'-'+du);

                ultimo = un+''+du+' '+v.mese_evento+' '+v.anno_evento;
                ultimo_en = '<span class="en">'+v.mese_evento+'</span> '+un+''+du+', '+v.anno_evento;

                // ultimo = v.data_evento.substring(0, 2)+' '+v.mese_evento+' '+v.anno_evento;
                // ultimo_en = '<span class="en">'+v.mese_evento+'</span> '+v.data_evento.substring(0, 2)+', '+v.anno_evento;

              }
// console.log(v.mese_evento+','+v.anno_evento_evento);
// console.log(v.mese_evento+','+v.anno_evento);
              // console.dir(arrmig);
              //console.log('numero')
              //console.dir(migranti_morti);

/* helpers qui! */

handlebars.registerHelper("normalizzami", function(input) {
  if ( input == "" || input == undefined || input == null ){
    output = "";
    return output;
  } else if ( input == "Non partecipante" ) {
    output = input.toString();
    return output.toLowerCase().replace(/'/g, "\-").replace(/\s+/g, '-');
  } else {
    // output = html_encode(input);
    input = input.replace(/[^\w ]/g, function(char) {
      return dict2[char] || char;
    });
    // input = iconv.encode(input, "UTF-8")
    return input;
    // output = htmlEncode(input);
    // return htmlDecode(output);
  }
});


        // read the file and use the callback to render
        fs.readFile('templates/foo.hbs', function(err, data){
          handlebars.registerHelper('contaimorti', function(tot_bambini, tot_donne, tot_uomini, tot_unknown, tot_morti) {
              _outmorti  = [];
              for (i=0; i<Number(tot_bambini); i++) {
                // _outmorti.push('<li><i class="bambini icon-icone-migranti" data-card="'+i+'" data-content="' + genPeople("b") + '"></i></li>');
                _outmorti.push('<li>');
              }
              for (i=0; i<Number(tot_donne); i++) {
                // _outmorti.push('<li><i class="donne icon-icone-migranti" data-card="'+i+'" data-content="' + genPeople("f") + '"></i></li>');
                _outmorti.push('<li>');
              }
              for (i=0; i<Number(tot_uomini); i++) {
                // _outmorti.push('<li><i class="uomini icon-icone-migranti" data-card="'+i+'" data-content="' + genPeople("m") + '"></i></li>');
                _outmorti.push('<li>');                
              }
              for (i=0; i<Number(tot_unknown); i++) {
                // _outmorti.push('<li><i class="unknown icon-icone-migranti" data-card="'+i+'" data-content="' + genPeople("m") + '"></i></li>');
                _outmorti.push('<li>');
              }
              shuffle(_outmorti);
              _outmorti = _outmorti.join(' ');
              return new handlebars.SafeString(
                _outmorti
              );
          });
          if (!err) {
            // make the buffer into a string
            var source = data.toString();
            // call the render function
            renderToString(source, migranti_morti, "include-elenco-morti_precompiled.html");
          } else {
            // handle file read error
          }
        });

        // read the file and use the callback to render
        fs.readFile('templates/fooM.hbs', function(err, data){
          handlebars.registerHelper('contaimorti', function(tot_bambini, tot_donne, tot_uomini, tot_unknown, tot_morti) {
              _outmorti  = [];
              for (i=0; i<Number(tot_bambini); i++) {
                // _outmorti.push('<li><i class="bambini icon-icone-migranti" data-card="'+i+'" data-content="' + genPeople("b") + '"></i></li>');
                _outmorti.push('<li>');
              }
              for (i=0; i<Number(tot_donne); i++) {
                // _outmorti.push('<li><i class="donne icon-icone-migranti" data-card="'+i+'" data-content="' + genPeople("f") + '"></i></li>');
                _outmorti.push('<li>');
              }
              for (i=0; i<Number(tot_uomini); i++) {
                // _outmorti.push('<li><i class="uomini icon-icone-migranti" data-card="'+i+'" data-content="' + genPeople("m") + '"></i></li>');
                _outmorti.push('<li>');                
              }
              for (i=0; i<Number(tot_unknown); i++) {
                // _outmorti.push('<li><i class="unknown icon-icone-migranti" data-card="'+i+'" data-content="' + genPeople("m") + '"></i></li>');
                _outmorti.push('<li>');
              }
              shuffle(_outmorti);
              _outmorti = _outmorti.join(' ');
              return new handlebars.SafeString(
                _outmorti
              );
          });
          if (!err) {
            // make the buffer into a string
            var source = data.toString();
            // call the render function
            renderToString(source, migranti_morti, "include-elenco-morti_precompiledM.html");
          } else {
            // handle file read error
          }
        });

        fs.readFile('templates/mortizone.hbs', function(err, data){
          
          if (!err) {
            // make the buffer into a string
            var source = data.toString();
            
            // call the render function
            renderToString(source, migranti_morti, "include-mortizone.html");
          } else {
            // handle file read error
          }
        });

        fs.readFile('templates/morti.hbs', function(err, data){
          
          if (!err) {
            // make the buffer into a string
            var source = data.toString();
            
            // call the render function
            renderToString(source, _morti_totali, "include-morti.html");
          } else {
            // handle file read error
          }
        });

        fs.readFile('templates/ultimo.hbs', function(err, data){
          
          if (!err) {
            // make the buffer into a string
            var source = data.toString();
            
            // call the render function
            renderToString(source, ultimo, "include-ultimo.html");
          } else {
            // handle file read error
          }
        });

        fs.readFile('templates/ultimo.hbs', function(err, data){
          
          if (!err) {
            // make the buffer into a string
            var source = data.toString();
            
            // call the render function
            renderToString(source, ultimo_en, "en/include-ultimo_en.html");
          } else {
            // handle file read error
          }
        });

        // this will be called after the file is read
        function renderToString(source, data, filename) {
          var template = handlebars.compile(source);
          var outputString = template(data);
          //apri_hanldetemple = '<div class="handletemple">';
          //chiudi_hanldetemple = '</div>';
          fs.writeFileSync(filename, "");
          //fs.appendFileSync(filename, apri_hanldetemple);
          fs.appendFileSync(filename, outputString);
          //fs.appendFileSync(filename, chiudi_hanldetemple);
          // console.log(outputString);
          // return outputString;
        }




    }
});

function genPeople(sesso){
  maschi = "cefghijqsHGFDC";
  femmine = "abxyzZEB";
  if(Math.random() >= 0.5){
    return( femmine.charAt(Math.floor(Math.random() * femmine.length)) );
  } else {
    return( maschi.charAt(Math.floor(Math.random() * maschi.length)) );
  }
 }
 function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;
  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}

function sortObject(obj, testVal) {
    var arr = [];
    var prop;

    for (prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push({
                'key': prop,
                'value': obj[prop][testVal]
            });
        }
    }

    arr.sort(function(a, b) {
        if (a.value > b.value) {
          return 1;
        }
        if (a.value < b.value) {
          return -1;
        }
        // a must be equal to b
        return 0;
    });


    return arr; // returns array
}



function compareData(a,b) {
  var dataA = a.data_evento;
  var dataB = a.data_evento;
  var dataA = dataA.split("/");
  var dataB = dataB.split("/");

  var A = new Date(dataA[2], dataA[1] - 1, dataA[0]);
  var B = new Date(dataB[2], dataB[1] - 1, dataB[0]);


  if (A < B)
    return -1;
  if (A > B)
    return 1;
  return 0;
}



/**
 * Encodes special html characters
 * @param string
 * @return {*}
 */
function html_encode(string) {
    var ret_val = '';
    for (var i = 0; i < string.length; i++) { 
        if (string.codePointAt(i) > 127) {
            ret_val += '&#' + string.codePointAt(i) + ';';
        } else {
            ret_val += string.charAt(i);
        }
    }
    return ret_val;
}

/* qui metto i dizionari che mi servono per convertire i caratteri speciali, 
occhio che nel template handlebars serve estrarre la variabile con 3 parentesi graffe per esportare l'html */
var dict = {
"á":"a",
"â":"a",
"ã":"a",
"ä":"a",
"ç":"c",
"è":"e",
"é":"e",
"ê":"e",
"ë":"e",
"ì":"i",
"í":"i",
"î":"i",
"ï":"i",
"ñ":"n",
"ò":"o",
"ó":"o",
"ô":"o",
"õ":"o",
"ö":"o",
"ù":"u",
"ú":"u",
"û":"u",
"ü":"u",
"ý":"y",
"ÿ":"y",
"À":"A",
"Á":"A",
"Â":"A",
"Ã":"A",
"Ä":"A",
"Ç":"C",
"È":"E",
"É":"E",
"Ê":"E",
"Ë":"E",
"Ì":"I",
"Í":"I",
"Î":"I",
"Ï":"I",
"Ñ":"N",
"Ò":"O",
"Ó":"O",
"Ô":"O",
"Õ":"O",
"Ö":"O",
"Ù":"U",
"Ú":"U",
"Û":"U",
"Ü":"U",
"à":"a",
"Ý":"Y"
}

var dict2 = {
"À":"&Agrave;",
"Á":"&Aacute;",
"Â":"&Acirc;",
"Ã":"&Atilde;",
"Ä":"&Auml;",
"Å":"&Aring;",
"Æ":"&AElig;",
"Ç":"&Ccedil;",
"È":"&Egrave;",
"É":"&Eacute;",
"Ê":"&Ecirc;",
"Ë":"&Euml;",
"Ì":"&Igrave;",
"Í":"&Iacute;",
"Î":"&Icirc;",
"Ï":"&Iuml;",
"Ð":"&ETH;",
"Ñ":"&Ntilde;",
"Ò":"&Ograve;",
"Ó":"&Oacute;",
"Ô":"&Ocirc;",
"Õ":"&Otilde;",
"Ö":"&Ouml;",
"×":"&times;",
"Ø":"&Oslash;",
"Ù":"&Ugrave;",
"Ú":"&Uacute;",
"Û":"&Ucirc;",
"Ü":"&Uuml;",
"Ý":"&Yacute;",
"Þ":"&THORN;",
"ß":"&szlig;",
"à":"&agrave;",
"á":"&aacute;",
"â":"&acirc;",
"ã":"&atilde;",
"ä":"&auml;",
"å":"&aring;",
"æ":"&aelig;",
"ç":"&ccedil;",
"è":"&egrave;",
"é":"&eacute;",
"ê":"&ecirc;",
"ë":"&euml;",
"ì":"&igrave;",
"í":"&iacute;",
"î":"&icirc;",
"ï":"&iuml;",
"ð":"&eth;",
"ñ":"&ntilde;",
"ò":"&ograve;",
"ó":"&oacute;",
"ô":"&ocirc;",
"õ":"&otilde;",
"ö":"&ouml;",
"÷":"&divide;",
"ø":"&oslash;",
"ù":"&ugrave;",
"ú":"&uacute;",
"û":"&ucirc;",
"ü":"&uuml;",
"ý":"&yacute;",
"þ":"&thorn;",
"ÿ":"&yuml;",
"¡":"&iexcl;",
"¢":"&cent;",
"£":"&pound;",
"¤":"&curren;",
"¥":"&yen;",
"¦":"&brvbar;",
"§":"&sect;",
"¨":"&uml;",
"©":"&copy;",
"ª":"&ordf;",
"«":"&laquo;",
"¬":"&not;",
"®":"&reg;",
"¯":"&macr;",
"°":"&deg;",
"±":"&plusmn;",
"²":"&sup2;",
"³":"&sup3;",
"´":"&acute;",
"µ":"&micro;",
"¶":"&para;",
"·":"&middot;",
"¸":"&cedil;",
"¹":"&sup1;",
"º":"&ordm;",
"»":"&raquo;",
"¼":"&frac14;",
"½":"&frac12;",
"¾":"&frac34;",
"€":"&euro;",
"‚":"&sbquo;",
"ƒ":"&fnof;",
"„":"&bdquo;",
"…":"&hellip;",
"†":"&dagger;",
"‡":"&Dagger;",
"ˆ":"&circ;",
"‰":"&permil;",
"Š":"&Scaron;",
"‹":"&lsaquo;",
"Œ":"&OElig;",
"Ž":"&Zcaron;",
"‘":"&lsquo;",
"’":"&rsquo;",
"“":"&ldquo;",
"”":"&rdquo;",
"•":"&bull;",
"–":"&ndash;",
"—":"&mdash;",
"˜":"&tilde;",
"™":"&trade;",
"š":"&scaron;",
"›":"&rsaquo;",
"œ":"&oelig;",
"ž":"&zcaron;",
"Ÿ":"&Yuml;",
"Ā":"&Amacr;",
"ā":"&amacr;",
"Ă":"&Abreve;",
"ă":"&abreve;",
"Ą":"&Aogon;",
"ą":"&aogon;",
"Ć":"&Cacute;",
"ć":"&cacute;",
"Ĉ":"&Ccirc;",
"ĉ":"&ccirc;",
"Ċ":"&Cdod;",
"ċ":"&cdot;",
"Č":"&Ccaron;",
"č":"&ccaron;",
"Ď":"&Dcaron;",
"ď":"&dcaron;",
"Đ":"&Dstrok;",
"đ":"&dstrok;",
"Ē":"&Emacr;",
"ē":"&emacr;",
"Ė":"&Edot;",
"ė":"&edot;",
"Ę":"&Eogon;",
"ę":"&eogon;",
"Ě":"&Ecaron;",
"ě":"&ecaron;",
"Ĝ":"&Gcirc;",
"ĝ":"&gcirc;",
"Ğ":"&Gbreve;",
"ğ":"&gbreve;",
"Ġ":"&Gdot;",
"ġ":"&gdot;",
"Ģ":"&Gcedil;",
"ģ":"&gcedil;",
"Ĥ":"&Hcirc;",
"ĥ":"&hcirc;",
"Ħ":"&Hstrok;",
"ħ":"&hstrok;",
"Ĩ":"&Itilde;",
"ĩ":"&itilde;",
"Ī":"&Imacr;",
"ī":"&imacr;",
"Į":"&Iogon;",
"į":"&iogon;",
"İ":"&Idot;",
"ı":"&inodot;",
"Ĳ":"&IJlog;",
"ĳ":"&ijlig;",
"Ĵ":"&Jcirc;",
"ĵ":"&jcirc;",
"Ķ":"&Kcedil;",
"ķ":"&kcedli;",
"ĸ":"&kgreen;",
"Ĺ":"&Lacute;",
"ĺ":"&lacute;",
"Ļ":"&Lcedil;",
"ļ":"&lcedil;",
"Ľ":"&Lcaron;",
"ľ":"&lcaron;",
"Ŀ":"&Lmodot;",
"ŀ":"&lmidot;",
"Ł":"&Lstrok;",
"ł":"&lstrok;",
"Ń":"&Nacute;",
"ń":"&nacute;",
"Ņ":"&Ncedil;",
"ņ":"&ncedil;",
"Ň":"&Ncaron;",
"ň":"&ncaron;",
"ŉ":"&napos;",
"Ŋ":"&ENG;",
"ŋ":"&eng;",
"Ō":"&Omacr;",
"ō":"&omacr;",
"Ő":"&Odblac;",
"ő":"&odblac;",
"Œ":"&OElig;",
"œ":"&oelig;",
"Ŕ":"&Racute;",
"ŕ":"&racute;",
"Ŗ":"&Rcedil;",
"ŗ":"&rcedil;",
"Ř":"&Rcaron;",
"ř":"&rcaron;",
"Ś":"&Sacute;",
"ś":"&sacute;",
"Ŝ":"&Scirc;",
"ŝ":"&scirc;",
"Ş":"&Scedil;",
"ş":"&scedil;",
"Š":"&Scaron;",
"š":"&scaron;",
"Ţ":"&Tcedil;",
"ţ":"&tcedil;",
"Ť":"&Tcaron;",
"ť":"&tcaron;",
"Ŧ":"&Tstrok;",
"ŧ":"&tstrok;",
"Ũ":"&Utilde;",
"ũ":"&utilde;",
"Ū":"&Umacr;",
"ū":"&umacr;",
"Ŭ":"&Ubreve;",
"ŭ":"&ubreve;",
"Ů":"&Uring;",
"ů":"&uring;",
"Ű":"&Udblac;",
"ű":"&udblac;",
"Ų":"&Uogon;",
"ų":"&uogon;",
"Ŵ":"&Wcirc;",
"ŵ":"&wcirc;",
"Ŷ":"&Ycirc;",
"ŷ":"&ycirc;",
"Ÿ":"&Yuml;",
"Ź":"&Zacute;",
"ź":"&zacute;",
"Ż":"&Zdot;",
"ż":"&zdot;",
"Ž":"&Zcaron;",
"ž":"&zcaron;",
"¿":"&iquest;"

}

